package com.example.mireaproject;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link nav_calculator#newInstance} factory method to
 * create an instance of this fragment.
 */
public class nav_calculator extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private EditText firstValue;
    private EditText operator;
    private EditText secondValue;

    public nav_calculator() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment nav_calculator.
     */
    // TODO: Rename and change types and number of parameters
    public static nav_calculator newInstance(String param1, String param2) {
        nav_calculator fragment = new nav_calculator();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nav_calculator, container, false);
        Button btn = (Button) view.findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                //onClickNumber(v);
                TextView output = (TextView) v.findViewById(R.id.outputString);
                firstValue = (EditText) v.findViewById(R.id.firstValue);
                operator = (EditText) v.findViewById(R.id.operator);
                secondValue = (EditText) v.findViewById(R.id.secondValue);

                int first = Integer.parseInt(firstValue.getText().toString());
                int oper = Integer.parseInt(operator.getText().toString());
                int second = Integer.parseInt(secondValue.getText().toString());
                System.out.println(first + "--------------------");
                int result = 0;
                if (oper=='+') {
                    result = first + second;
                }else
                {
                    if (oper=='-') {
                        result = first - second;
                    }else
                    {
                        if (oper=='*') {
                            result = first * second;
                        }else
                        {
                            if (oper=='/') {
                                result = first / second;
                            }else
                            {
                                    result = first+second+1;
                            }
                        }
                    }
                }
                if (result == first+second+1){
                    output.setText("Error");
                }else{
                output.setText(result);
                }
            }
        });
        return view;
    }
}